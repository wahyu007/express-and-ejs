const express = require('express');
let path = require('path');
const app = express();
const port = 3000;
let bodyParser = require('body-parser')



// const logger = (req, res, next) => {
//     console.log(`${req.method} ${req.url}`)
//     next()
// }

// app.use(logger)

const errorStatus = (req, res, next) => {
    res.status(404).json({
        status : "error",
        message : "Are You Lost?"
    })
}

// view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//setup public folder
app.use(express.static('./public'));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.render('pages/home')
});


let messages = (req, res, next) => {
    let message;
    console.log(message);
    res.locals.message = message;
    next();
}

app.get('/form', messages, (req, res) => {
    // console.log(messages)
    res.render('pages/form');
});

app.post('/form', (req, res) => {
    let message = req.body;
    console.log(message);
    res.locals.message = message;
    res.render('pages/form');
    // res.json({
    //     status: "muncul",
    //     messages: req.body
    // })
});

app.get('/links', (req, res) => {
    let items = [
        {
            name : 'node.js',
            url : 'https://nodejs.org/en'
        },
        {
            name : 'ejs',
            url : 'https://ejs.co'
        },
        {
            name : 'expressjs',
            url : 'https://expressjs.com'
        },
        {
            name : 'vuejs',
            url : 'https://vuejs.org'
        },
        {
            name : 'nextjs',
            url : 'https://nextjs.org'
        },
    ];
    res.render('pages/links', {
        links:items
    })
})

app.get('/table', (req, res) => {
    let items = [
        {
            name : 'node.js',
            url : 'https://nodejs.org/en'
        },
        {
            name : 'ejs',
            url : 'https://ejs.co'
        },
        {
            name : 'expressjs',
            url : 'https://expressjs.com'
        },
        {
            name : 'vuejs',
            url : 'https://vuejs.org'
        },
        {
            name : 'nextjs',
            url : 'https://nextjs.org'
        },
    ];
    res.render('pages/table', {
        table:items
    })
})

app.get('/list', (req, res) => {
    // array with items to send
    let items = ['node.js', 'expressjs', 'ejs', 'javascript', 'bootstrap'];
    res.render('pages/list', {
        list:items
    })
});

app.use(errorStatus);
app.listen(port, () => console.log(`MasterEJS app stated on port ${port}!`))